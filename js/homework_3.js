// Теория
// 1. Если в коде есть операции, которые повторяются, то можно их записать с помощью функции.
// Тем самым упрощается работа и код становится меньше и чище.
// 2. Аргументы задаются в том случае, когда они играют роль в теле функции для её выполнения.
// Они по сути являются локальными переменными

function calculator() {
    let userNumberFirst = +prompt('Введите первое число');
    let userNumberSecond = +prompt('Введите второе число');
    let userOperation = prompt('Введите математическую операцию +,  -,  *  или /');
    let answer;

    switch (userOperation) {
        case '+':
            answer = userNumberFirst + userNumberSecond;
            break;
        case '-':
            answer = userNumberFirst - userNumberSecond;
            break;
        case '*':
            answer = userNumberFirst * userNumberSecond;
            break;
        case '/':
            answer = userNumberFirst / userNumberSecond;
            break;
        default:
            alert('Такой математической операции не найдено. Повторите ещё раз.');
            break;
    }
    console.log(answer);
}

calculator();